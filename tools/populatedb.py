# coding=utf-8
import os
import pymongo

client = pymongo.MongoClient("localhost", 27017)
db = client.shaadiforless
db.users.drop()
db.post.drop()
db.words.drop()


os.system('mongoimport --jsonArray --db shaadiforless --collection users --file ../data/users.json')
os.system('mongoimport --jsonArray --db shaadiforless --collection post --file ../data/post.json')
