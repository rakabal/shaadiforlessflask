from app import db

class User(db.Document):
    #nickname = db.StringField(verbose_name="NickName", max_length=64, required=True)
    email = db.StringField(verbose_name="Name", max_length=120, required=True)
    #posts = db.relationship('Post', backref='author', lazy='dynamic')

    def __repr__(self):
        return '<User %r>' % (self.nickname)

class Post(db.Document):
    body = db.StringField(required=True)
    timestamp = db.DateTimeField(default=datetime.datetime.now, required=True)
    user_id = db.StringField(verbose_name="Name", max_length=255, required=True)

    def __repr__(self):
        return '<Post %r>' % (self.body)