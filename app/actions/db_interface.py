from flask import jsonify, g, current_app
import pymongo
from bson import json_util
from bson.objectid import ObjectId
import json



# Connect to Mongo DB
def connect(db_name):
    client = pymongo.MongoClient("localhost", 27017)
    db = client[db_name]
    db.users.ensure_index([('name', 'text')],username="search_index",weights={'name':100})

    return db