from flask import Flask
import pymongo
from app.actions import db_interface

app = Flask(__name__)
app.config.from_object('config')
db = db_interface.connect(app.config['DATABASE'])

from app import views, models